# -*- coding: utf-8 -*-
import urllib.request
from bs4 import BeautifulSoup
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from selenium import webdriver
from slack.web.classes import extract_json
from slack.web.classes.blocks import *


SLACK_TOKEN = 'xoxb-689652889920-678240675282-tfol2r875mqjvEPO2sWvXkNo'
SLACK_SIGNING_SECRET = '860a7050a6d8fb89711882d737e55a2d'

app = Flask(__name__)
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

_search_url = ""
_running_url = ""

# 랭킹 장르 바꾸기
def _change_genre(text):
    """
    https: // movie.naver.com / movie / sdb / rank / rmovie.nhn?sel = pnt & date = 20190710 & tg = 0
    전체 0
    드라마 1
    판타지 2
    공포 4
    멜로/애정/로맨스 5
    모험 6
    스릴러 7
    느와르 8
    다큐멘터리 10
    코미디 11
    가족 12
    미스터리 13
    전쟁 14
    애니메이션 15
    범죄 16
    뮤지컬 17
    SF 18
    액션 19
    """

    # 장르 번호를 입력받고 (입력예시 장르 2 or 장르 3) 장르로 url 바꾸기
    # 앞에 장르 글자 빼고 숫자만 해서 url 수정\

    if '전체' in text:
        genre_num = 0
    elif '드라마' in text:
        genre_num = 1
    elif '판타지' in text:
        genre_num = 2
    elif '공포' in text:
        genre_num = 4
    elif '멜로' in text:
        genre_num = 5
    elif '모험' in text:
        genre_num = 6
    elif '스릴러' in text:
        genre_num = 7
    else:
        genre_num = 0

    url = 'https://movie.naver.com/movie/sdb/rank/rmovie.nhn?sel=pnt&date=20190710&tg=' + str(genre_num)

    return url

#추천페이지 크롤링
def _crawl_rec(text):
    url = _change_genre(text)
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    keywords = []

    cnt = 1
    keywords.append("높은 평점순 추천작입니다.")
    for data in (soup.find_all('td', class_='title')):
        if cnt > 20:
            break
        keywords.append(str(cnt) + "위 " + data.find('div', class_='tit5').get_text().rstrip('\n')[1:])
        cnt = cnt+1

    # 키워드 리스트를 문자열로 만듭니다.
    return u'\n'.join(keywords)

#현재 상영영화 리스트 크롤링
def _now_running(text):

    global _running_url
    list_href = []
    url = 'https://movie.naver.com/movie/running/current.nhn'
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    _running_url = url


    keywords = []
    keywords.append("현재 상영작입니다. 원하시는 영화의 자세한 정보를 원하신다면 번호로 검색해주세요!\nex)1위 영화 검색 시 : @Movie_Bot 1")
    for href in soup.find("ul", class_="lst_detail_t1").find_all("li"):
        list_href.append("https://movie.naver.com/" + href.find("a")["href"])
    cnt = 1
    for data in (soup.find('ul', class_='lst_detail_t1').find_all('li')):
        if cnt >= 11:
            break
        keywords.append("<"+list_href[cnt-1]+"|" + str(cnt) + "위 " + data.find('dt', class_='tit').find('a').get_text()+">")
        cnt = cnt+1

    # 키워드 리스트를 문자열로 만듭니다.
    return u'\n'.join(keywords)

#상영영화 중 원하는 영화 정보 크롤링
def _running_moive_data(text) :
    list_content = []
    list_href = []
    global _running_url
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('headless')
    driver = webdriver.Chrome(r'C:\Users\student\Downloads\chromedriver_win32\chromedriver',
                              chrome_options=chrome_options)
    driver.implicitly_wait(3)
    driver.get(_running_url)
    driver.implicitly_wait(3)

    html = driver.page_source
    soup = BeautifulSoup(html)

    for href in soup.find("ul", class_="lst_detail_t1").find_all("li"):
        list_href.append("https://movie.naver.com/" + href.find("a")["href"])

    url = list_href[int(text[13:]) - 1]
    driver1 = webdriver.Chrome(r'C:\Users\student\Downloads\chromedriver_win32\chromedriver',
                               chrome_options=chrome_options)
    driver.implicitly_wait(3)
    driver1.get(url)
    driver.implicitly_wait(3)

    html1 = driver1.page_source
    soup = BeautifulSoup(html1)
    _image_soup = soup
    data = soup.find("div", class_="wide_info_area").find("div", class_="mv_info")
    list_content.append(data.find("h3", class_="h_movie").find("a").get_text())

    data = soup.find("div", class_="wide_info_area").find("div", class_="mv_info")
    list_content.append(
        "기자·평론가 : " + data.find("div", class_="main_score").find("div", class_='spc_score_area').find("div",
                                                                                                    class_="star_score").get_text().strip() + "점 , 네티즌 : "
        + data.find("div", class_="main_score").find_all("div", class_='score')[2].find("div",
                                                                                        class_="star_score").get_text().strip()+ "점")
    data = soup.find("div", class_="wide_info_area").find("div", class_="mv_info")
    actor = data.find("div", class_="info_spec2").find("dl", class_="step2").get_text()
    list_content.append(actor.replace('\t', '').replace('\n', ''))

    data = soup.find("div", class_="section_group section_group_frst").find("div", class_="obj_section")
    list_content.append("영화 줄거리 : ")
    list_content.append(data.find("div", class_="story_area").find("p", class_="con_tx").get_text())

    return u'\n'.join(list_content)

#영화 검색 크롤링
def _search_movies(text):
    global _search_url
    list_href = []
    text = text.strip().encode('EUC-KR')
    text = str(text).replace('\\','%').replace('x','')[15:-1].upper()
    url = "https://movie.naver.com/movie/search/result.nhn?section=movie&query=" + text
    _search_url = url

    i = 1

    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    for href in soup.find("ul", class_="search_list_1").find_all("li"):
        list_href.append("https://movie.naver.com/" + href.find("a")["href"])

    movie_list = []
    for data in soup.find("ul", class_="search_list_1").find_all("li"):
        movie_list.append("<"+list_href[i-1]+"|"+str(i)+ " " +  data.find("dt").find("a").get_text()+">")
        i += 1
        if i >= 10 :
            break
    movie_list.append("더 자세한 영화 정보를 알고 싶으신 영화의 번호를 선택해주세요! \nex)2번째 영화 검색 시 : @Movie_Bot 검색2")
    return u'\n'.join(movie_list)

#검색한 영화 중 원하는 영화 정보 크롤링
def _search_movie_data(text) :
    list_content = []
    list_href = []
    global _search_url
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('headless')
    driver = webdriver.Chrome(r'C:\Users\student\Downloads\chromedriver_win32\chromedriver', chrome_options = chrome_options)
    driver.implicitly_wait(3)
    driver.get(_search_url)
    driver.implicitly_wait(3)

    html = driver.page_source
    soup = BeautifulSoup(html)
    for href in soup.find("ul", class_="search_list_1").find_all("li"):
        list_href.append("https://movie.naver.com/" + href.find("a")["href"])

    url = list_href[int(text[15:])-1]
    driver1 = webdriver.Chrome(r'C:\Users\student\Downloads\chromedriver_win32\chromedriver',chrome_options = chrome_options)
    driver.implicitly_wait(3)
    driver1.get(url)
    driver.implicitly_wait(3)

    html1 = driver1.page_source
    soup = BeautifulSoup(html1)
    data = soup.find("div", class_="wide_info_area").find("div", class_="mv_info")
    list_content.append(data.find("h3", class_="h_movie").find("a").get_text())

    data = soup.find("div", class_="wide_info_area").find("div", class_="mv_info")
    check_data = data.find("div", class_="main_score")
    if check_data == None :
        pass
    else :
        list_content.append("기자'평론가 " + data.find("div", class_="main_score").find("div",class_='spc_score_area').find("div",class_ = "star_score").get_text().strip() + ", 네티즌 "
                        + data.find("div", class_="main_score").find_all("div", class_='score')[2].find("div",class_="star_score").get_text().strip())

    data = soup.find("div", class_="wide_info_area").find("div", class_="mv_info")
    actor = data.find("div", class_="info_spec2").find("dl", class_="step2").get_text()
    list_content.append(actor.replace('\t','').replace('\n',''))

    data = soup.find("div", class_="section_group section_group_frst").find("div", class_="obj_section")
    list_content.append("영화 줄거리")
    list_content.append(data.find("div", class_="story_area").find("p", class_="con_tx").get_text())

    return u'\n'.join(list_content)

#상영작 포스터 크롤링
def _running_image_crawl(text) :
    global _running_url
    image_url = ""
    list_href = []
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('headless')
    driver = webdriver.Chrome(r'C:\Users\student\Downloads\chromedriver_win32\chromedriver',
                              chrome_options=chrome_options)
    driver.implicitly_wait(3)
    driver.get(_running_url)
    driver.implicitly_wait(3)

    html = driver.page_source
    soup = BeautifulSoup(html)

    for href in soup.find("ul", class_="lst_detail_t1").find_all("li"):
        list_href.append("https://movie.naver.com/" + href.find("a")["href"])

    url = list_href[int(text[13:]) - 1]
    driver1 = webdriver.Chrome(r'C:\Users\student\Downloads\chromedriver_win32\chromedriver',
                               chrome_options=chrome_options)
    driver.implicitly_wait(3)
    driver1.get(url)
    driver.implicitly_wait(3)

    html1 = driver1.page_source
    soup = BeautifulSoup(html1)
    data = soup.find("div", class_="wide_info_area").find("div", class_="poster").find("a")
    image_url = data.find("img").get("src")
    return image_url

#검색 영화 포스터 크롤링
def _search_image_crawl(text) :
    global _search_url
    image_url = ""
    list_href = []
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('headless')
    driver = webdriver.Chrome(r'C:\Users\student\Downloads\chromedriver_win32\chromedriver',
                              chrome_options=chrome_options)
    driver.implicitly_wait(3)
    driver.get(_search_url)
    driver.implicitly_wait(3)

    html = driver.page_source
    soup = BeautifulSoup(html)

    for href in soup.find("ul", class_="search_list_1").find_all("li"):
        list_href.append("https://movie.naver.com/" + href.find("a")["href"])

    url = list_href[int(text[15:]) - 1]
    driver1 = webdriver.Chrome(r'C:\Users\student\Downloads\chromedriver_win32\chromedriver',
                               chrome_options=chrome_options)
    driver.implicitly_wait(3)
    driver1.get(url)
    driver.implicitly_wait(3)

    html1 = driver1.page_source
    soup = BeautifulSoup(html1)
    data = soup.find("div", class_="wide_info_area").find("div", class_="poster").find("a")
    image_url = data.find("img").get("src")
    return image_url

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    if text[13:].strip().isalpha():
        if "상영영화" in text :
            message = _now_running(text)
            slack_web_client.chat_postMessage(
                channel=channel,
                text=message
            )
        elif "영화추천" in text :
            if '장르' in text:
                message = _crawl_rec(text)
                slack_web_client.chat_postMessage(
                    channel=channel,
                    text=message
                )
            else:
                slack_web_client.chat_postMessage(
                    channel=channel,
                    text='장르를 선택해주세요\n전체 / 드라마 / 판타지 / 공포 / 멜로 / 모험 / 스릴러\nex)영화추천장르전체 영화추천장르드라마'
                )
        else :
            message = _search_movies(text)
            slack_web_client.chat_postMessage(
                channel=channel,
                text=message
            )

    elif text[-1].strip().isdigit():
        if "검색" in text:
            block2 = SectionBlock(
                text=_search_movie_data(text)
            )
            # 이미지 블록: 큰 이미지 하나를 표시합니다..
            block3 = ImageBlock(
                image_url=_search_image_crawl(text),
                alt_text="이미지가 안 보일 때 대신 표시할 텍스트"
            )
            # 여러 개의 블록을 하나의 메시지로 묶어 보냅니다.
            my_blocks = [block3, block2]
            slack_web_client.chat_postMessage(
                channel=channel,
                blocks=extract_json(my_blocks)
            )

        else :
            block2 = SectionBlock(
                text = _running_moive_data(text)
            )

            # 이미지 블록: 큰 이미지 하나를 표시합니다..
            block3 = ImageBlock(
                image_url= _running_image_crawl(text),
                alt_text="이미지가 안 보일 때 대신 표시할 텍스트"
            )

            # 여러 개의 블록을 하나의 메시지로 묶어 보냅니다.
            my_blocks = [block3, block2]
            slack_web_client.chat_postMessage(
                channel=channel,
                blocks=extract_json(my_blocks)
            )

# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"

if __name__ == '__main__':
    app.run('127.0.0.1', port=4040)